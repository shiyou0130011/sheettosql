package main

import (
	"flag"
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/shiyou0130011/sheettosql"
)

func main() {
	var file, path, outDir, table, db string
	var isReplace = false
	var genRunAll = false

	flag.StringVar(&file, "f", "", "讀取的 sheet")
	flag.StringVar(&path, "dir", "", "讀取的資料夾，如果有設置 -f 則此參數無效")
	flag.StringVar(&table, "t", "", "要變更的 table")
	flag.BoolVar(&isReplace, "replace", false, "是否使用 Replace Query")
	flag.StringVar(&db, "db", "", "轉成 SQL 後，要執行的該 SQL 的資料庫")
	flag.BoolVar(&genRunAll, "runall", false, "是否生成 Run All 的檔案")
	flag.StringVar(&outDir, "out", "", "輸出檔案的資料夾，如果沒有就自動於 temp 資料夾生成")
	flag.Parse()

	if file == "" && path == "" {

		flag.PrintDefaults()

		return
	}

	sheettosql.IsInsert = !isReplace
	sheettosql.Table = table

	if outDir == "" {
		var err error
		outDir, err = ioutil.TempDir(os.TempDir(), table+"-sheet")
		if err != nil {
			return
		}
		log.Println("Do not have output dir setting")
		log.Printf("Generating output dir as %s", outDir)
	}

	if file != "" {
		log.Printf("== %s ==", "Start generating file")
		sheettosql.Load(file, outDir)
	} else {
		log.Printf("== %s ==", "Start generating folder")
		dir, err := os.Open(path)
		if err != nil {
			log.Fatalf("Error: %v", err)
			return
		}

		fis, err := dir.Readdir(-1)
		if err != nil {
			log.Fatalf("Error: %v", err)
			return
		}

		for _, fi := range fis {
			filePath := path + "/" + fi.Name()
			sheettosql.Load(filePath, outDir)
		}
	}

	if genRunAll {
		sheettosql.GenRunAll(outDir)
	}

}
