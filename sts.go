package sheettosql

import (
	"fmt"
	"log"
	"os"
	"strings"
)

func Load(fileAddress string, outDir string) {
	if strings.ToLower(fileAddress[len(fileAddress)-4:]) == "xlsx" {
		err := loadXLSX(fileAddress, outDir)

		if err != nil {
			log.Fatalf("%v", err)
		}
	} else if strings.ToLower(fileAddress[len(fileAddress)-3:]) == "csv" {
		err := loadCSV(fileAddress, outDir)
		if err != nil {
			log.Fatalf("%v", err)
		}
	} else {
		log.Fatalf("Error: Does not support the file %s", fileAddress)
	}

}

func GenRunAll(outDir string) {
	log.Printf("== Start Generating run all file ==")
	defer log.Printf("== Finish Generating run all file ==")

	const runAllFileName = "run-all.sql"

	os.Remove(outDir + "/" + runAllFileName)

	var allSQLRunFile *os.File
	allSQLRunFile, err := os.Create(outDir + "/" + runAllFileName)
	if err != nil {
		log.Fatalf("Error, %v", err)
		return
	} else {
		defer allSQLRunFile.Close()
	}
	outDirect, _ := os.Open(outDir)
	fis, err := outDirect.Readdir(-1)
	if err != nil {
		log.Fatalf("%v", err)
	}

	for _, fi := range fis {
		if !fi.IsDir() && strings.Contains(fi.Name(), ".sql") && fi.Name() != runAllFileName {
			allSQLRunFile.WriteString(fmt.Sprintf("select 'compile %v' as info;\n", fi.Name()))
			allSQLRunFile.WriteString(fmt.Sprintf("source %s/%v\n", outDir, fi.Name()))
		} else {
			log.Printf("Warn: Skip file %s", fi.Name())
		}

	}

	log.Printf("Run All file generated")
	log.Printf("File: %s", outDir+"/"+"run-all.sql")
}

// 處理 sheet column 欄位的值
//
// 如果其值為 NULL ，則視同 SQL value 為 NULL。
//
//
func handleColumnValue(col string) string {
	if col == "NULL" {
		return col
	}

	// 因為是 SQL ，所以用單引號表示字串
	// 此外，由於 ` 是 SQL 關鍵字，所以加斜線
	result := strings.ReplaceAll(
		strings.ReplaceAll(
			strings.ReplaceAll(
				strings.ReplaceAll(
					strings.ReplaceAll(
						col,
						`\`, `\\`,
					),
					"'", "''",
				),
				`"`, `\"`,
			),
			"`", "\\`",
		),
		"\n", `\n`,
	)

	return "'" + result + "'"
}
