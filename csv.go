package sheettosql

import (
	"bytes"
	"encoding/csv"
	"errors"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
)

func loadCSV(file string, outputDir string) error {
	f, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	_, fileName := filepath.Split(file)

	query, err := generateCSVSheetToQuery(csv.NewReader(bytes.NewBuffer(f)))
	if err != nil {
		return err
	}

	outFile, err := ioutil.TempFile(outputDir, Table+"-*.sql")
	if err != nil {
		return err
	}

	defer outFile.Close()
	log.Printf("Generate sheet data %-64s to %s", fileName, outFile.Name())

	outFile.WriteString(`-- ` + fileName + "\n")
	if _, err = outFile.WriteString(query); err != nil {
		return err
	}

	return nil
}

func generateCSVSheetToQuery(r *csv.Reader) (string, error) {

	csvRows, err := r.ReadAll()
	if err != nil {
		return "", err
	}

	if len(csvRows) < 1 {
		return "", errors.New("CSV file does not have rows")
	}
	insertRows := make([]string, len(csvRows)-1)

	for i, row := range csvRows {
		if i == 0 {
			continue
		}

		insertRowData := make([]string, len(row))
		for colIndex, col := range row {
			insertRowData[colIndex] = handleColumnValue(col)

		}

		insertRows[i-1] = `(` + strings.Join(insertRowData, ",") + `)`

	}
	query := ""
	if IsInsert {
		query += "INSERT IGNORE INTO `" + Table + "`\n"
	} else {
		query += "REPLACE INTO `" + Table + "`\n"
	}

	query += "(`" + strings.Join(csvRows[0], "`,`") + "`)\nVALUES\n" + strings.Join(insertRows, ",\n")
	return query, nil
}
