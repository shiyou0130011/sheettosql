package sheettosql

import (
	"fmt"
	"io/ioutil"
	"log"
	"path"
	"path/filepath"
	"strings"

	"github.com/tealeg/xlsx"
)

const null = "NULL"

func loadXLSX(file string, outputDir string) error {
	xfile, err := xlsx.OpenFile(file)

	if err != nil {
		return err
	}

	done := make(chan bool)

	for sheetIndex, s := range xfile.Sheets {
		go (func(d chan bool, sheet *xlsx.Sheet, sheetIndex int) {
			if sheet.Hidden {
				log.Printf("%s is hidden. Skip the sheet", sheet.Name)
				d <- false
				return
			}

			log.Printf("== Start Generate %s ==", sheet.Name)
			defer (func() {
				log.Printf("== Finish Generate %s ==", sheet.Name)
				d <- true
			})()

			outputFileName := ""
			if getFileName(file) == sheet.Name {
				outputFileName = fmt.Sprintf("%s-Sheet%02d", getFileName(file), sheetIndex)
			} else {
				outputFileName = fmt.Sprintf("%s-%s", getFileName(file), sheet.Name)
			}

			outFile, err := ioutil.TempFile(outputDir, outputFileName+"-*.sql")

			if err != nil {
				log.Printf("Error: %v", err)
				return
			}

			defer outFile.Close()

			log.Printf("Generate sheet data to %s", outFile.Name())

			query := generateXLSXSheetToQuery(sheet)

			if _, err = outFile.WriteString("-- " + sheet.Name + "\n"); err != nil {
				log.Printf("Error: %v", err)
			}
			if _, err = outFile.WriteString(query); err != nil {
				log.Printf("Error: %v", err)
			}
		})(done, s, sheetIndex)

	}
	for i := 0; i < len(xfile.Sheets); i++ {
		<-done
	}

	return nil
}

func generateXLSXSheetToQuery(sheet *xlsx.Sheet) string {
	columnNames := []string{}

	colQuery := ""

	for index, row := range sheet.Rows {
		if index == 0 {
			// Fisrt Row, used for column name
			for _, col := range row.Cells {
				columnNames = append(columnNames, col.Value)
			}
			continue
		}

		if index != 1 {
			colQuery += ",\n"
		}

		cols := []string{}

		for _, col := range row.Cells {
			cols = append(cols, handleColumnValue(col.Value))
		}

		colQuery += fmt.Sprintf(`(%s)`, strings.Join(cols, ", "))
	}

	query := ""
	if IsInsert {
		query += "INSERT IGNORE INTO `" + Table + "`\n"
	} else {
		query += "REPLACE INTO `" + Table + "`\n"
	}
	query += fmt.Sprintf("(`%s`)", strings.Join(columnNames, "`, `")) + "\nVALUES\n" + colQuery

	return query

}

func getFileName(filePath string) string {
	_, f := filepath.Split(filePath)
	return strings.Replace(f, path.Ext(f), "", -1)
}
