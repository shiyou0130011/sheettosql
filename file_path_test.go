package sheettosql

import (
	"testing"
)

func TestGetFileName(t *testing.T) {
	filePath := `C:\user\Temp\foo\data.txt`

	if result := getFileName(filePath); result != "data" {
		t.Fatalf("Error: Result is %s", result)
	}

}
