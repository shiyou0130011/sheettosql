package sheettosql

import "testing"

func Test_handleColumnValue(t *testing.T) {
	var testVal []string = []string{
		`This is a book named "John's book".`,
		"Hello World. \nToday is a good day.",
		"SELECT * from `some-table` WHERE `id` > '0';",
		"NULL",
		"Null",
		`{"a":1, b: [1,3,4], c: "a&b"}`,
		`\(^o^)/`,
	}

	for _, s := range testVal {
		t.Logf("Test String: %s", s)
		t.Logf("Result     : %s", handleColumnValue(s))
		t.Log("")
	}

}
